# Laravel auth test

## Requirements

- docker
- docker-compose

## Usage
For start up the app - follow next commands from folder root:

- Copy .env.example to .env (/src)
- `docker-compose up -d --build site`
- `docker-compose run --rm composer install`
- `docker-compose run --rm npm install`
- `docker-compose run --rm npm run dev`
- `docker-compose run --rm artisan migrate`

### Site available on http://0.0.0.0:8080/ 
