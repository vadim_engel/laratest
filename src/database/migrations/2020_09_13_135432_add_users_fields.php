<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUsersFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('phone')->unique()->index('user_phone');
            $table->integer('approach')->nullable();
            $table->string('certificate_number')->nullable();
            $table->string('diploma_number')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('phone');
            $table->dropIndex('user_phone');
            $table->dropColumn('approach');
            $table->dropColumn('certificate_number');
            $table->dropColumn('diploma_number');
        });
    }
}
