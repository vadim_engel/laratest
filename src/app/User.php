<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    const APPROACH_BACHELOR = 1;
    const APPROACH_MASTER = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone', 'approach', 'certificate_number', 'diploma_number'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Получение направлений обучения
     *
     * @return array
     */
    public static function getAllApproaches()
    {
        return [self::APPROACH_BACHELOR => 'Бакалавр', self::APPROACH_MASTER => 'Магистр'];
    }

    public function getApproach()
    {
        return $this->approach ? self::getAllApproaches()[$this->approach] : null;
    }
}
