@extends('layouts.app')

@push('scripts')
    <script src="{{ asset('js/register.js') }}" defer></script>
@endpush

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="phone" class="col-md-4 col-form-label text-md-right">{{ __('Phone') }}</label>

                            <div class="col-md-6">
                                <input id="phone" type="tel" pattern="(\+?\d[- .]*){5,18}" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" required autocomplete="phone">

                                @error('phone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="approach" class="col-md-4 col-form-label text-md-right">{{ __('Approach') }}</label>

                            <div class="col-md-6">
                                <select name="approach" id="approach" class="form-control @error('approach') is-invalid @enderror">
                                    @foreach($approaches as $key => $approach)
                                        <option value="{{ $key }}" @if(old('approach') == $key) selected @endif>{{ $approach }}</option>
                                    @endforeach
                                </select>

                                @error('approach')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row" @if(old('approach') && old('approach') != \App\User::APPROACH_BACHELOR) style="display: none" @endif data-approach="{{ \App\User::APPROACH_BACHELOR }}">
                            <label for="certificate_number" class="col-md-4 col-form-label text-md-right">{{ __('Certificate number') }}</label>

                            <div class="col-md-6">
                                <input id="certificate_number" type="text" class="form-control @error('certificate_number') is-invalid @enderror" name="certificate_number" value="{{ old('certificate_number') }}" autocomplete="certificate_number">

                                @error('certificate_number')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row" @if(old('approach') != \App\User::APPROACH_MASTER) style="display: none" @endif data-approach="{{ \App\User::APPROACH_MASTER }}">
                            <label for="diploma_number" class="col-md-4 col-form-label text-md-right">{{ __('Diploma number') }}</label>

                            <div class="col-md-6">
                                <input id="diploma_number" type="text" class="form-control @error('diploma_number') is-invalid @enderror" name="diploma_number" value="{{ old('diploma_number') }}" autocomplete="diploma_number" >

                                @error('diploma_number')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
