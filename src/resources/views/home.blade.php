@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="col">
                        <p>Phone: {{ $user->phone }}</p>
                        <p>Approach: {{ $user->approach_text }}</p>
                        @if($user->approach === \App\User::APPROACH_BACHELOR)
                        <p>Certificate number: {{ $user->certificate_number }}</p>
                        @elseif($user->approach === \App\User::APPROACH_MASTER)
                        <p>Diploma number: {{ $user->diploma_number }}</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
