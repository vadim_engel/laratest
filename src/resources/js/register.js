'use strict';

document.addEventListener('DOMContentLoaded', function() {
    document.getElementById('approach').addEventListener('change', (event) => {
        const approach = parseInt(event.target.value);
        const inputs = document.querySelectorAll('[data-approach]');

        inputs.forEach((item) => {
            const itemApproach = parseInt(item.dataset.approach);
            if (itemApproach === approach) {
                item.style = '';
            } else {
                item.style.display = 'none';
            }
        });
    });
});
